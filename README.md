Heartland Medical Sales and Services has been a trusted supplier of new and refurbished capital medical equipment since 1998. We offer and supply a variety of medical devices such as anesthesia machines, defibrillators, ultrasound equipment, surgical tables, electrosurgical units & patient monitors.

Address: 2601 Holloway Road, Louisville, KY 40299, USA

Phone: 502-671-1014

Website: https://www.heartlandmedical.com
